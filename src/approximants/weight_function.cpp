/*
 * ExplicitSim - Software for solving PDEs using explicit methods.
 * Copyright (C) 2017  <Konstantinos A. Mountris> <konstantinos.mountris@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors (alphabetically):
 *      George C. BOURANTAS
 *      Grand R. JOLDES
 *      Konstantinos A. MOUNTRIS
 *      Benjamin F. ZWICK
 */

#include "ExplicitSim/approximants/weight_function.hpp"

#include <cmath>

namespace ExplicitSim {

double MostBucher2005WeightFunction::value(const double r)
{
    double e = this->epsilon_;

    // Equation (26) in Most and Bucher (2005) --- see also Equation (30)
    return (pow(pow(r, 2) + e, -2) - pow(1 + e, -2)) / (pow(e, -2) - pow(1 + e, -2));
}

double MostBucher2005WeightFunction::dwdr_div_r(const double r)
{
    double e = this->epsilon_;

    // Derivative of Equation (26) wrt r in Most and Bucher (2005)
    // Compare with Equation (31a); here r = d/D
    return -4. * pow(r*r + e, -3) / (pow(e, -2) - pow(1 + e, -2));
}

double QuarticSplineWeightFunction::value(const double r)
{
    return 1. - 6.*r*r + 8.*r*r*r - 3.*r*r*r*r;
}

double QuarticSplineWeightFunction::dwdr_div_r(const double r)
{
    return - 12. + 24.*r - 12.*r*r;
}

} //end of namespace ExplicitSim
