/*
 * ExplicitSim - Software for solving PDEs using explicit methods.
 * Copyright (C) 2017  <Konstantinos A. Mountris> <konstantinos.mountris@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors (alphabetically):
 *      George C. BOURANTAS
 *      Grand R. JOLDES
 *      Konstantinos A. MOUNTRIS
 *      Benjamin F. ZWICK
 */

/*!
   \file weight.hpp
   \brief Weight function base class header file.
   \author Benjamin F. Zwick
   \date 05/09/2023
*/

#ifndef EXPLICITSIM_APPROXIMANTS_WEIGHT_FUNCTION_HPP_
#define EXPLICITSIM_APPROXIMANTS_WEIGHT_FUNCTION_HPP_

namespace ExplicitSim {

/*!
 *  \addtogroup ShapeFunctions
 *  @{
 */


/*!
 * \class WeightFunction
 * \brief Base class implementing weight functions.
 */

class WeightFunction
{
public:

    virtual ~WeightFunction() {}

    /// @brief Compute the ith element of the weight function vector
    /// @param r (distance)
    /// @return
    virtual double value(const double r) = 0;

    /// @brief Compute derivative of the weight function wrt to distance divided by distance.
    /// @param r (distance)
    /// @return
    virtual double dwdr_div_r(const double r) = 0;
};

/*!
 * \class MostBucher2005WeightFunction
 * \brief An almost interpolating moving least squares weight function introduced by Most & Bucher (2005).

References:

  - Most, T., and Bucher, C. (2005).
    A Moving Least Squares weighting function
    for the Element-free Galerkin Method
    which almost fulfills essential boundary conditions.
    Structural Engineering and Mechanics 21, 315-332.
 */

class MostBucher2005WeightFunction : public WeightFunction
{
public:
    virtual double value(const double r);
    virtual double dwdr_div_r(const double r);

private:
    /// Regularization parameter; See Equation (27) in Most & Bucher (2005)
    double epsilon_ = 1e-5;
};

/*!
 * \class QuarticSplineWeightFunction
 * \brief Quartic spline weight function.
 */

class QuarticSplineWeightFunction : public WeightFunction
{
public:
    virtual double value(const double r);
    virtual double dwdr_div_r(const double r);
};

/*! @} End of Doxygen Groups*/
} //end of namespace ExplicitSim

#endif //EXPLICITSIM_APPROXIMANTS_WEIGHT_FUNCTION_HPP_
