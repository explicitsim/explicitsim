/*
 * ExplicitSim - Software for solving PDEs using explicit methods.
 * Copyright (C) 2017  <Konstantinos A. Mountris> <konstantinos.mountris@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors (alphabetically):
 *      George C. BOURANTAS
 *      Grand R. JOLDES
 *      Konstantinos A. MOUNTRIS
 */



/*!
   \file solvers.hpp
   \brief ExplicitSim solvers module's header file. Collection of PDE solvers' header files.
   \author Konstantinos A. Mountris
   \date 27/09/2017
*/

#ifndef EXPLICITSIM_SOLVERS_SOLVERS_HPP_
#define EXPLICITSIM_SOLVERS_SOLVERS_HPP_

// Collection of PDE solvers' header files.

#include "ExplicitSim/solvers/dyn_relax_prop.hpp"
#include "ExplicitSim/solvers/mtled.hpp"

#endif //EXPLICITSIM_SOLVERS_SOLVERS_HPP_

