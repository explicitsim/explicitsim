/*
 * ExplicitSim - Software for solving PDEs using explicit methods.
 * Copyright (C) 2017  <Konstantinos A. Mountris> <konstantinos.mountris@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors (alphabetically):
 *      George C. BOURANTAS
 *      Grand R. JOLDES
 *      Konstantinos A. MOUNTRIS
 */


/*!
   \file elements.hpp
   \brief Header file collecting the header files of the various elements classes.
   \author Konstantinos A. Mountris
   \date 16/11/2017
*/

#ifndef EXPLICITSIM_ELEMENTS_ELEMENTS_HPP_
#define EXPLICITSIM_ELEMENTS_ELEMENTS_HPP_

#include "ExplicitSim/elements/element_properties.hpp"
#include "ExplicitSim/elements/node.hpp"
#include "ExplicitSim/elements/tetrahedron.hpp"
#include "ExplicitSim/elements/surface.hpp"

#endif //EXPLICITSIM_ELEMENTS_ELEMENTS_HPP_
