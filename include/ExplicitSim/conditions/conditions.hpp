/*
 * ExplicitSim - Software for solving PDEs using explicit methods.
 * Copyright (C) 2017  <Konstantinos A. Mountris> <konstantinos.mountris@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors (alphabetically):
 *      George C. BOURANTAS
 *      Grand R. JOLDES
 *      Konstantinos A. MOUNTRIS
 */



/*!
   \file conditions.hpp
   \brief ExplicitSim conditions module's header file. Collection of conditions (boundary, loading) header files.
   \author Konstantinos A. Mountris
   \date 30/09/2017
*/

#ifndef EXPLICITSIM_CONDITIONS_CONDITIONS_HPP_
#define EXPLICITSIM_CONDITIONS_CONDITIONS_HPP_

// Collection of conditions (boundary, loading) header files.

#include "ExplicitSim/conditions/conditions_handler.hpp"
#include "ExplicitSim/conditions/load_curve.hpp"

#endif //EXPLICITSIM_CONDITIONS_CONDITIONS_HPP_
