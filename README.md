# README #

### What is this repository for? ###

ExplicitSim is a software for solving PDEs using explicit methods.


### How do I get set up? ###

You need to compile ParaView using CMake. 
This will give you access to the required libraries (VTK, Boost) for compiling ExplicitSim.

For instructions on how to install ExplicitSim
please refer to the [Installation Guide](./INSTALL.md).
