import configparser
import os
import sys

model = configparser.RawConfigParser()
model.optionxform = str

# ------------------------------------------------------------------------------
# Default values used for all models

model['Model'] = {
    'MeshFile': '',             # parameter (see below)
    'MassScaling': '0',
}

model['IntegrationOptions'] = {
    'Adaptive': '0',
    'AdaptiveEps': '0.1',
    'AdaptiveLevel': '10',
    'TetrahedronDivisions': '4',
    'IntegPointsPerTetrahedron': '1',
}

# Material is defined during loop
model['Material'] = {}          # parameter (see below)

model['ShapeFunction'] = {
    'Type': 'mmls',
    'BasisFunctionType': 'quadratic',
    'UseExactDerivatives': '1',
    'DilatationCoefficient': '1.6',
}

model['Boundary'] = {
    'FixedName': 'Fixed',
    'FixedAxes': '1 1 1',
}

model['Loading'] = {
    'LoadName': 'Displaced',
    'LoadAxes': '1 1 1',
    'Displacement': '',         # parameter (see below)
    'LoadCurve': 'smooth',
}

model['EBCIEM'] = {
    'UseEBCIEM': '1',
    'UseSimplifiedVersion': '0',
}

model['DynamicRelaxation'] = {
    'LoadTime': '2.0',
    'EquilibriumTime': '0.5',
    'LoadConvRate': '1.0',
    'AfterLoadConvRate': '1.0',
    'StopUpdateConvRateStepsNum': '1000',
    'ConvRateDeviation': '0.0001',
    'ForceDispUpdateStepsNum': '100',
    'StableConvRateStepsNum': '30',
    'ConvRateStopDeviation': '0.002',
    'StopConvRateError': '0.2',
    'StopAbsError': '0.1',
    'StopStepsNum': '500',
}

model['MTLED'] = {
    'SaveProgressSteps': '500',
    'UsePredefinedStableTimeStep': '0',
    'StableTimeStep': '0.00000001',
}

model['Output'] = {}            # parameter (see below)

# ------------------------------------------------------------------------------
# Materials

materials = {
    'neohookean': {
        'Material': {
            'Type': 'neohookean',
            'Density': '1000.',
            'YoungModulus': '1916.58',
            'PoissonRatio': '0.49',
        },
    },
    'ogden': {
        'Material': {
            'Type': 'ogden',
            'Density': '1000.',
            'Mu': '639.9',
            'Alpha': '-1.1',
            'D1': '3.16539e-05',
        },
    },
}

# ------------------------------------------------------------------------------
# Boundary conditions

bcs = {
    'compression': {
        'MeshFile': 'gel_cylinder_compression_mesh_c3d4.inp',
        'Displacement': '0  0 -0.01',
    },
    'indentation': {
        'MeshFile': 'gel_cylinder_indentation_mesh_c3d4.inp',
        'Displacement': '0  0 -0.0135',
    },
}

# ------------------------------------------------------------------------------
# Write the input files

prefix = 'gel_cylinder'

for mname, material in materials.items():
    for bname, bc in bcs.items():
        # Generate file name
        filename = prefix + '_' + bname + '_' + mname

        # Modify the model
        model['Material'] = material['Material']
        model['Model']['MeshFile'] = bc['MeshFile']
        model['Loading']['Displacement'] = bc['Displacement']
        model['Output'] = {
            'FilePath': './' + filename + '/',
            'FileName': filename + '.vtu',
            'AnimationName': filename + '.pvd',
        }

        # Check if file exists (do not overwrite)
        configfile = filename + '.ini'
        if os.path.exists(configfile):
            print("File '{}' exists. Overwrite (y/[n])? ".format(configfile), end='')
            if not 'y' == input().lower():
                print("Exiting without writing input file.")
                sys.exit(1)

        # Write the file
        with open(configfile, 'w') as f:
            model.write(f)
