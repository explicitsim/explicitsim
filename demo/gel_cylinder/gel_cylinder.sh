#!/usr/bin/env sh

# Run the gel cylinder demos

ExplicitSimRun gel_cylinder_compression_neohookean.ini
ExplicitSimRun gel_cylinder_compression_ogden.ini

ExplicitSimRun gel_cylinder_indentation_neohookean.ini
ExplicitSimRun gel_cylinder_indentation_ogden.ini
