#!/usr/bin/env sh

# Run the tissue extension demos

ExplicitSimRun tissue_extension_coarse.ini
ExplicitSimRun tissue_extension_fine.ini
